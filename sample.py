import torch

tensor = torch.tensor([1, -2, 3, 4, -5, 6, -7, 8])
positive_indexes = torch.arange(tensor.size(0), dtype = torch.long, device = tensor.device)
negative_indexes = torch.arange(tensor.size(0), dtype = torch.long, device = tensor.device)

positive_indexes = positive_indexes[tensor >= 0]
negative_indexes = negative_indexes[tensor < 0]

print(positive_indexes)
print(negative_indexes)
