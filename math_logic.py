import numpy as np
## Machine 1
w1 = []
g1 = [0.163, 0.149, -0.125, 0.35, -0.25, 0.227, -0.36]


## Machine 2
w2 = []
g2 = [0.165, 0.152, -0.12, 0.32, -0.22, 0.21, -0.34]

## Machine 3
w3 = []
g3 = [0.168, 0.172, -0.33, 0.42, -0.26, 0.16, -0.42]

## Machine 4
w4 = []
g4 = [0.15, 0.184, -0.32, 0.28, -0.30, 0.31, -0.32]

## This is the empirical workflow of my work
## Machine 1

##Indexes
ing1, ing2, ing3, ing4 = [i for i in range(len(g1)) if g1[i] < 0], [i for i in range(len(g2)) if g2[i] < 0], [i for i in range(len(g3)) if g3[i] < 0], [i for i in range(len(g4)) if g4[i] < 0]

ipg1, ipg2, ipg3, ipg4 = [i for i in range(len(g1)) if g1[i] >= 0], [i for i in range(len(g2)) if g2[i] >= 0], [i for i in range(len(g3)) if g3[i] >= 0], [i for i in range(len(g4)) if g4[i] >= 0]
# Divide + and -
ng1, ng2, ng3, ng4 = [g1[i] for i in range(len(g1)) if g1[i] < 0], [g2[i] for i in range(len(g2)) if g2[i] < 0], [g3[i] for i in range(len(g3)) if g3[i] < 0], [g4[i] for i in range(len(g4)) if g4[i] < 0]
pg1, pg2, pg3, pg4 = [g1[i] for i in range(len(g1)) if g1[i] >= 0], [g2[i] for i in range(len(g2)) if g2[i] >= 0], [g3[i] for i in range(len(g3)) if g3[i] >= 0], [g4[i] for i in range(len(g4)) if g4[i] >= 0]
# Calculate Mean
nm1, nm2, nm3, nm4 = np.mean(ng1), np.mean(ng2), np.mean(ng3), np.mean(ng4)
pm1, pm2, pm3, pm4= np.mean(pg1), np.mean(pg2), np.mean(pg3), np.mean(pg4)


# Save Residuals
nr1, nr2, nr3, nr4 = [nm1 - ng1[i] for i in range(len(ng1))], [nm2 - ng2[i] for i in range(len(ng2))], [nm3 - ng3[i] for i in range(len(ng3))], [nm4 - ng4[i] for i in range(len(ng4))]
pr1, pr2, pr3, pr4 = [pm1 - pg1[i] for i in range(len(pg1))], [pm2 - pg2[i] for i in range(len(pg2))], [pm3 - pg3[i] for i in range(len(pg3))], [pm4 - pg4[i] for i in range(len(pg4))]

# All reduce
nm = (nm1 + nm2 + nm3 + nm4)/4
pm = (pm1 + pm2 + pm3 + pm4)/4

# Generate new Gradients
u1, u2, u3, u4 = [0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0]
u1, u2, u3, u4 = u1[nr1[ing1[i]]+nm for i in range(len(nr1))], [nr2[ing2[i]]+nm for i in range(len(nr2))], [nr3[ing3[i]]+nm for i in range(len(nr3))], [nr4[ing4[i]]+nm for i in range(len(nr4))]

print("U1: %s       U2: %s      U3: %s      U4: %s" %(u1, u2, u3, u4))
# Apply (w = w - l*g)  --  Not l*g always


##This is the usual workflow

# All reduce
u1, u2, u3, u4 = [(g1[i] + g2[i] + g3[i] + g4[i])/4 for i in range(4)], [(g1[i] + g2[i] + g3[i] + g4[i])/4 for i in range(4)], [(g1[i] + g2[i] + g3[i] + g4[i])/4 for i in range(4)], [(g1[i] + g2[i] + g3[i] + g4[i])/4 for i in range(4)]
print("U1: %s       U2: %s      U3: %s      U4: %s" %(u1, u2, u3, u4))
# Apply (w = w - l*g)  --  Not l*g always

