import torch
import numpy as np
import math
class BucketCompressor():
    """
    Bucket compressor by Subhadeep
    """
    buckets = []
    means = []
    residuals = []
    name = 'bucket'                                         #Name of compressor

    @staticmethod
    def clear():
        BucketCompressor.buckets = []
        BucketCompressor.means = []
        BucketCompressor.residuals = []

    @staticmethod
    def sign_bucket_mean(tensor, bin_coun = None):
        ''' 
        This is a straight froward implementation using only two buckets 
        Divide the bucket based on sign    
        '''
        bin_count = 2

        BucketCompressor.buckets = [] # Init buckets
        BucketCompressor.means = torch.zeros(2, dtype=tensor.dtype, device=tensor.device) #Init means list with two zeros
        BucketCompressor.residuals = torch.zeros_like(tensor) #Init residual - size same as tensor


        positive_indexes = torch.arange(tensor.size(0), dtype = torch.long, device = tensor.device) #Init positive indexes -- size same as tensor
        negative_indexes = torch.arange(tensor.size(0), dtype = torch.long, device = tensor.device) #Init negative indexes -- size same as tensor

        BucketCompressor.buckets.append(positive_indexes[tensor >= 0])  #Filter indexes with positive values from tensor and append it to bucket
        BucketCompressor.buckets.append(negative_indexes[tensor < 0])   #Filter indexes with negative values from tensor and append it to bucket

        for i in range(bin_count):  # Itearte twice and fill the mean and bucket
            if len(BucketCompressor.buckets[i]) > 0: # Safety check for empty bucket -- either positive size or negative side exists
                BucketCompressor.means[i] = torch.mean(tensor[BucketCompressor.buckets[i]], dim=-1) #Calculate mean of each bucket
                tensor[BucketCompressor.buckets[i]] = tensor[BucketCompressor.buckets[i]].add_(-BucketCompressor.means[i]) # Update tensor values

        BucketCompressor.residuals = tensor #save the updated tensor as residuals -- May need to discard this later as original tensor value updated

        return BucketCompressor.means   #Return mean array which will be used for allreduce

    @staticmethod
    def automated_bucket_mean(tensor, bucket_count = None):
        '''
        This is an automated approach where bucket is determined based on the decimal part
        First divide the entire tensor in positive and negative bucket
        Then divide positive and negative part further using decimal part
        '''

        BucketCompressor.buckets = [] # Init buckets
        BucketCompressor.residuals = torch.zeros_like(tensor) # Init residuals
        tensor_size = tensor.numel() # Fetch size of the tensor

        max_value = torch.max(tensor).int() # Decimal part of max tensor value
        min_value = torch.min(tensor).int() # Decimal part of min tensor value
        bucket_count = torch.abs(max_value) + torch.abs(min_value) + 2 # Total bucket count

        positive_indexes = torch.arange(tensor.size(0), dtype = torch.long, device = tensor.device)
        negative_indexes = torch.arange(tensor.size(0), dtype = torch.long, device = tensor.device)
        positive_indexes = positive_indexes[tensor >= 0] # set positive indexes
        negative_indexes = negative_indexes[tensor < 0] # set negative indexes


        positive_count = positive_indexes.numel() # Positive value count
        negative_count = negative_indexes.numel() # Negative value count

        temp_var = 0

        positive_bins = [x for x in range(max_value + 1)]   # generate positive bins
        negative_bins = [x for x in range(min_value, 1)]    # generate negative bins
         
        for p in range(len(positive_bins)): # Iterate to positive bins and fill the bucket with corresponding indexes
            temp_bucket  = torch.arange(positive_indexes.size(0), dtype = torch.long, device = tensor.device)
            BucketCompressor.buckets.append(temp_bucket[tensor[positive_indexes].int() == positive_bins[p]])


        for n in range(len(negative_bins)): # Iterate through negative bins and fill the bucket with corresponding values
            temp_bucket = torch.arange(negative_indexes.size(0), dtype = torch.long, device = tensor.device)
            BucketCompressor.buckets.append(temp_bucket[tensor[negative_indexes].int() == negative_bins[n]])


        BucketCompressor.means = torch.zeros(bucket_count, dtype=tensor.dtype, device=tensor.device) # Initialize means list - size is equal to # of buckets

        for i in range(bucket_count):                   #Iterate through each bucket and fill the mean and bucket
            if len(BucketCompressor.buckets[i]) > 0:    #If the bucket is not empty
                BucketCompressor.means[i] = torch.mean(tensor[BucketCompressor.buckets[i]], dim=-1) #Calculate mean of each bucket 
                tensor[BucketCompressor.buckets[i]] = tensor[BucketCompressor.buckets[i]].add_(-BucketCompressor.means[i]) #Update tensor values

        BucketCompressor.residuals = tensor #Store the updated tensor as residuals -- May need to remove later

        return BucketCompressor.means       #Return mean array which will be used to allreduce 

    @staticmethod
    def uniform_bucket_mean(tensor, bin_count = None):
        '''
        This approach first divide the positive and negative indexes
        Then it divides the positive and negative beans uniformly 
        '''
        BucketCompressor.buckets = [[] for i in range(bin_count)] # Init buckets
        BucketCompressor.residuals = torch.zeros_like(tensor) # Init residuals
        tensor_size = tensor.numel() # Fetch size of the tensor
        BucketCompressor.means = torch.zeros(bin_count, dtype=tensor.dtype, device=tensor.device) # Initialize means list - size is equal to # of buckets

        sorted_tensor_indexes = tensor.argsort() # Argsort the entire tensor in ascending order
        sorted_tensor = tensor[sorted_tensor_indexes] # Sorted tensor values

        positive_indexes = torch.arange(tensor.size(0), dtype = torch.long, device = tensor.device)
        negative_indexes = torch.arange(tensor.size(0), dtype = torch.long, device = tensor.device)
        #print(sorted_tensor)
        
        positive_indexes = sorted_tensor_indexes[positive_indexes[sorted_tensor >= 0]] # set positive indexes
        negative_indexes = sorted_tensor_indexes[negative_indexes[sorted_tensor < 0]] # set negative indexes

        positive_count = positive_indexes.numel()
        negative_count = negative_indexes.numel()

        positive_bin_count = round((positive_count/tensor_size) * bin_count)
        negative_bin_count = round((negative_count/tensor_size) * bin_count)

        positive_bin_capacity = math.ceil((positive_count + positive_bin_count - 1)/positive_bin_count)
        negative_bin_capacity = math.ceil((negative_count + negative_bin_count - 1)/negative_bin_count)

        #print(positive_indexes, negative_indexes)
        #print(tensor[positive_indexes])
        #print(tensor[negative_indexes])
        
        counter = 0
        start = 0
        while start < positive_count:
            end = start + positive_bin_capacity
            bucket = positive_indexes[start:end]
            BucketCompressor.buckets[counter] = bucket
            start = start + positive_bin_capacity
            counter += 1
        start = 0

        while start < negative_count:
            end = start + negative_bin_capacity
            bucket = negative_indexes[start:end]
            BucketCompressor.buckets[counter] = bucket
            start = start + negative_bin_capacity
            counter += 1


        #print("Buckets:%s" %(BucketCompressor.buckets))
        #print("Positive: %s Negative: %s Total: %s" %(positive_bin_count, negative_bin_count, bin_count))
        for i in range(bin_count):                   #Iterate through each bucket and fill the mean and bucket
            if len(BucketCompressor.buckets[i]) > 0:    #If the bucket is not empty
                #print(tensor[BucketCompressor.buckets[i]])
                BucketCompressor.means[i] = torch.mean(tensor[BucketCompressor.buckets[i]], dim=-1) #Calculate mean of each bucket
                tensor[BucketCompressor.buckets[i]] = tensor[BucketCompressor.buckets[i]].add_(-BucketCompressor.means[i]) #Update tensor values

        BucketCompressor.residuals = tensor #Store the updated tensor as residuals -- May need to remove later

        return BucketCompressor.means

    @staticmethod
    def compress(tensor, name=None):
        with torch.no_grad():
            # Bucket implementation
            updated_tensor = BucketCompressor.uniform_bucket_mean(tensor.data, 5) #Set all bucket related information
            return tensor, None, updated_tensor            # Original gradients, None, mean values

    @staticmethod
    def decompress(tensor, name=None):
        for i in range(len(BucketCompressor.means)):                                             #Iterate through each allreduced mean values
            BucketCompressor.residuals[BucketCompressor.buckets[i]] = BucketCompressor.residuals[BucketCompressor.buckets[i]].add_(tensor[i]) #Add mean values with each element in a bucket

        tensor = BucketCompressor.residuals
        BucketCompressor.clear()

        return tensor


def main():
    tensor = torch.tensor([1.8, -1.4, -0.1, 0.4, -0.5, 0.6, -0.7, -1.8]).cuda()
    print(tensor)
    tensor, ctx, selected_tensor = BucketCompressor.compress(tensor)
    
    print(selected_tensor)
    print(BucketCompressor.decompress(selected_tensor))


if __name__ == '__main__':
    main()
